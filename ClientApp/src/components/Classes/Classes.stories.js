import React from 'react';

import Classes from './Classes';

export default {
  title: 'Components/Classes',
  component: Classes,
};

export const Primary = function Primary() {
  return <Classes />;
};
