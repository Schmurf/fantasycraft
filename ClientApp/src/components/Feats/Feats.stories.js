import { rest } from 'msw';
import React from 'react';

import feats from '../../mockData/feats';
import Feats from './Feats';

export default {
  title: 'Components/Feats',
  component: Feats,
};

export const Primary = function Primary() {
  return <Feats />;
};

Primary.parameters = {
  msw: [rest.get('/feats', (req, res, ctx) => res(ctx.json(feats)))],
};
