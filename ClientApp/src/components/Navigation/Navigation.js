import React from 'react';
import { Container, Nav, Navbar } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

const Navigation = function Navigation() {
  const { t } = useTranslation();
  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand>Fantasy Craft</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={Link} to="/">
              {t('navigation.home')}
            </Nav.Link>
            <Nav.Link as={Link} to="/races">
              {t('navigation.races')}
            </Nav.Link>
            <Nav.Link as={Link} to="/classes">
              {t('navigation.classes')}
            </Nav.Link>
            <Nav.Link as={Link} to="/feats">
              {t('navigation.feats')}
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Navigation;
