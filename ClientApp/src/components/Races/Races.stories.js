import { rest } from 'msw';
import React from 'react';

import races from '../../mockData/races';
import Races from './Races';

export default {
  title: 'Components/Races',
  component: Races,
};

export const Primary = function Primary() {
  return <Races />;
};

Primary.parameters = {
  msw: [rest.get('/races', (req, res, ctx) => res(ctx.json(races)))],
};
