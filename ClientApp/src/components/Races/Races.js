import React, { useEffect, useState } from 'react';
import { Table } from 'react-bootstrap';

const Races = function Races() {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);

  useEffect(() => {
    fetch('/races')
      .then((res) => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setItems(result);
        },
        (err) => {
          setIsLoaded(true);
          setError(err);
        },
      );
  }, []);
  if (error) {
    return <div>Erreur :{error.message}</div>;
  }
  if (!isLoaded) {
    return <div>Chargement...</div>;
  }
  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>Races</th>
          <th>Caractéristiques</th>
          <th>Avantages</th>
        </tr>
      </thead>
      <tbody>
        {items?.map((item) => (
          <tr key={item.name}>
            <th>{item.name}</th>
            <th>{item.attributes}</th>
            <th>{item.advantages}</th>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export default Races;
