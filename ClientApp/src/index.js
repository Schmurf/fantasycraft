import 'bootstrap/dist/css/bootstrap.min.css';

import './i18n';

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import App from './App';
import Classes from './components/Classes/Classes';
import Feats from './components/Feats/Feats';
import Home from './components/Home/Home';
import Races from './components/Races/Races';
import worker from './mocks/browser';
import registerServiceWorker from './registerServiceWorker';

if (process.env.NODE_ENV === 'development') {
  worker.start();
}
const baseUrl = document.getElementsByTagName('base')[0].getAttribute('href');
const rootElement = document.getElementById('root');

ReactDOM.render(
  <BrowserRouter basename={baseUrl}>
    <App />
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/classes" element={<Classes />} />
      <Route path="/races" element={<Races />} />
      <Route path="/feats" element={<Feats />} />
    </Routes>
  </BrowserRouter>,
  rootElement,
);

registerServiceWorker();
