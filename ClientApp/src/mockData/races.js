const races = [
  {
    name: 'Drake',
    attributes: '+2 For ou Int, -2 Dex',
    advantages:
      'Bête de Grande Taille (Allonge 2, blessures Con *1.5, Vitesse 6 cases), attaques naturelles (Morsure 1, Griffe 1), curieux, honni, sang froid, souffle (feu), vol ailé (10 cases)',
  },
  {
    name: 'Nain',
    attributes: '+4 Con, -2 Dex',
    advantages:
      "Taille Moyenne, (Allonge 1, blessures Con, Vitesse: 4 cases), actions restreintes (Coup de pied, Saut, Natation), artisanat, cuir épais 2, stabilité, tripes d'acier, vision dans le noir 1",
  },
];

export default races;
