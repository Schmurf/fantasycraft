import { rest } from 'msw';

import feats from '../mockData/feats';
import races from '../mockData/races';

const handlers = [
  rest.get('/races', (req, res, ctx) => res(ctx.status(200), ctx.json(races))),
  rest.get('/feats', (req, res, ctx) => res(ctx.status(200), ctx.json(feats))),
];

export default handlers;
