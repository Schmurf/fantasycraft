import './custom.css';

import React from 'react';

import Navigation from './components/Navigation/Navigation';

const App = function App() {
  return <Navigation />;
};

export default App;
